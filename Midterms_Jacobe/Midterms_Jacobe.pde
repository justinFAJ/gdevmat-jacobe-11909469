void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  bHole.x = random(-640, 640);
  bHole.y = random(-360, 360);
  bHole = new PVector(bHole.x, bHole.y);
  
  for(int i = 0; i < colorMatter.length; i++)
  {
   colorMatter[i] = new Mover();
  }
}

PVector bHole = new PVector();
Mover[] colorMatter = new Mover[100];

void draw()
{
  background(0, 0, 0);
  
  for(int i = 0; i < colorMatter.length; i++)
  {
    colorMatter[i].move();
    colorMatter[i].render();
  }
  
  fill(255, 255, 255);
  circle(bHole.x, bHole.y, 50);
  println(frameCount);
  
  if (frameCount == 300)
  {
    clear();
    frameCount = 0;
    setup();
  }
}
