public class Mover
{
  float gaussian = randomGaussian();
  float standardDeviation = 800;
  float mean = 0;
  float y = random(-Window.windowHeight / 2, Window.windowHeight / 2);
  float x = (standardDeviation * gaussian + mean) + y;
  PVector matter = new PVector(x, y);
  float r = random (0, 255), g = random (0, 255), b = random (0, 255);
  float thickness = random (2, 40);
  
  void move()
  {
    PVector magnetize = PVector.sub(bHole, matter);
    matter.add(magnetize.normalize().mult(10));
  }
  
  void render()
  {
    noStroke();
    fill(r, g, b);
    circle(matter.x, matter.y, thickness);
  }
}
