class Walker
{
  PVector position = new PVector();
  PVector speed = new PVector(5, 8);
  
  void moveAndBounce()
  {
    background(128, 128, 128);
    
    position.add(speed);
    if ((position.x > Window.right) || (position.x < Window.left))
    {
      speed.x *= -1;
      fill(random(0, 255), random(0, 255), random(0, 255), random(50, 100));
    }
    if ((position.y > Window.top) || (position.y < Window.bottom))
    {
      speed.y *= -1;
       fill(random(0, 255), random(0, 255), random(0, 255), random(50, 100));
    }
  }
  void render()
  {
    circle(position.x, position.y, 50);
  }
}
