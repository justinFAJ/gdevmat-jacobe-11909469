public class Walker
{
  PVector position = new PVector(-500, 200);
  PVector velocity = new PVector();
  PVector acceleration = new PVector();
  
  public float velocityLimit = 10;
  public float scale = 15;
  public float r = random (0, 255), g = random (0, 255), b = random (0, 255);
  public float mass = 1;


  public Walker()
  {
  }

  public void applyForce(PVector force)
  {
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
  
  public void update()
  {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    this.velocity.limit(velocityLimit);
    this.acceleration.mult(0);
  }
  
  public void render()
  {
    circle(position.x, position.y, scale);
  }
  
  public void colorBall()
  {
    fill(r, g, b);
  }
}
