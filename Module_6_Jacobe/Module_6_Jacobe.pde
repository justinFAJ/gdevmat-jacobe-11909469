void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}

void draw()
{
  background (128, 128, 128);
  
  PVector saber = mousePos();
  PVector saber2 = mousePos();
  PVector hilt = mousePos();
  PVector hilt2 = mousePos();
  PVector hilt3 = mousePos();
  PVector hilt4 = mousePos();
  
  saber.normalize().mult(450);
  strokeWeight(10);
  stroke(220, 20, 60);
  line(0, 0, (saber.x), (saber.y));
  strokeWeight(5);
  stroke(250, 250, 250);
  line(0, 0, (saber.x), saber.y);
  
  saber2.normalize().mult(-450);
  strokeWeight(10);
  stroke(220, 20, 60);
  line(0, 0, (saber2.x), (saber2.y));
  strokeWeight(5);
  stroke(250, 250, 250);
  line(0, 0, (saber2.x), saber2.y);
  
  hilt.normalize().mult(80);
  strokeWeight(15);
  //stroke(169, 169, 169); If you were to go with a silver hilt.
  stroke(0, 0, 0);
  line(0, 0, (hilt.x), (hilt.y));
  
  hilt2.normalize().mult(-80);
  strokeWeight(15);
  //stroke(169, 169, 169); If you were to go with a silver hilt.
  stroke(0, 0, 0);
  line(0, 0, (hilt2.x), (hilt2.y));
  
  hilt3.normalize().mult(-5);
  strokeWeight(20);
  stroke(169, 169, 169);
  line(0, 0, (hilt3.x), (hilt3.y));
  
  hilt4.normalize().mult(5);
  strokeWeight(20);
  stroke(0, 0, 0);
  line(0, 0, (hilt4.x), (hilt4.y));
  
  println(saber.mag());
  
}
