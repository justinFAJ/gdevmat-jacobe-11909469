void setup()
{
  size(1020, 720, P3D);
  //camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0); *Removed because the paint splashes weren't centered*
  background(255);
}

void draw()
{
  float gaussian = randomGaussian();
  float scaleGaussian = randomGaussian();
  
  float standardDeviation = 800; // Scatters values.
  float mean = 0;
  float y = random(height); // Randomizes the y value.
  float x = (standardDeviation * gaussian + mean) + y;
  float scaleMean = 0;
  float scaleStandardDeviation = 50;
  float scale = scaleStandardDeviation * scaleGaussian + scaleMean;
  
  fill(random(0, 255), random(0, 255), random(0, 255), random(10, 100));
  noStroke();
  circle(x, y, scale);
  
  // Flushes the screen after 300 frames. 
  println(frameCount);
  if (frameCount == 300) 
  {
    clear ();
    background(255);
  }
}
