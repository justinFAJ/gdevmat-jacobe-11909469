void setup()
{
  size (1280, 720, P3D);
  background(255);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

Walker perlinWalker = new Walker();

float x;
float y;
float z;
float a;
float tx = 0;
float ty = 100;
float tz = 150;
float ta = 250;

void draw()
{
  noStroke();
  // Increments
  tx += 0.1f;
  ty += 0.1f;
  tz += 0.1f;
  ta += 0.1f;
  
  // Color generation // mapping
  fill(map(noise(tx), 0, 1, 0, 255),
  map(noise(ty), 0, 1, 0, 255),
  map(noise(tz), 0, 1, 0, 255), 255);
  
  // Functions
  perlinWalker.render();
  perlinWalker.perlinWalk();
  
  //Made to circle on the middle of the screen.
  println(frameCount);
  if (frameCount == 1)
  {
    clear();
    background(255);
  }
}
