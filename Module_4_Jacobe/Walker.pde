public class Walker
{
  public float x = 0, y = 0;
  public float tx = 0, ty = 10000;
  public float tz = 500;
  public float scale = 5;
  
  void render()
  {
    scale = map(noise(tz), 0, 1, 5, 150);
    tz += 0.01f;
    circle(x, y, scale);
  }
  
  void perlinWalk()
  {
    x = map(noise(tx), 0, 1, -640, 640);
    y = map(noise(ty), 0, 1, -360, 360);
   
    tx += 0.01f;
    ty += 0.01f;
  }
}
