void setup()
{
  size(1020, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  
  noStroke();
}

Walker myWalker = new Walker(); // First walker
Walker secondWalker = new Walker(); //econd walker

void draw()
{
  // Calls Function
  myWalker.randomWalk();
  secondWalker.randomWalkBiased();
  
  // Renders the walkers
  myWalker.render(); 
  secondWalker.render();
  
   // Generates the colors
  fill(random(0,225), random(0,225), random(0,255), random(50, 100));
}
