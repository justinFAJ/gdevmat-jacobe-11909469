class Walker
{
    float x;
    float y;
    
    void render()
    {
      circle(x, y, 30);
    }
    //First Walker
    void randomWalk()
    {
      // Random Number Generator
      int rng = int(random(8));
      
      // Up
      if (rng == 0)
      {
        y+=10;
      }
      // Down
      else if (rng == 1)
      {
        y-=10;
      }
      // Right
      else if (rng == 2)
      {
        x+=10;
      }
      // Left
      else if (rng == 3)
      {
        x-=10;
      }
      // Up + Right
      else if (rng ==4)
      {
        y+=10;
        x+=10;
      }
      // Down + Right
      else if  (rng == 5)
      {
        y-=10;
        x+=10;
      }
      // Up + Left
      else if (rng == 6)
      {
        y+=10;
        x-=10;
      }
      // Down + Left
      else if (rng == 7)
      {
        y-=10;
        x-=10;
      }
    }
    // Second Walker
    void randomWalkBiased()
    {
      //Random Number Generator
      int rng = int (random(5));
      
      // Up
      if (rng == 0)
      {
        y+=2;
      }
      // Down
      else if (rng == 1)
      {
        y-=2;
      }
      // Right
      else if (rng <= 5)
      {
        x+=1;
      }
      // Left
      else if (rng == 3)
      {
        x-=2;
      }
    }
}
