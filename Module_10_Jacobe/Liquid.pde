class Liquid 
{
  float x, y, w, h, c;

  Liquid(float x_, float y_, float w_, float h_, float c_) 
  {
    x = x_;
    y = y_;
    w = w_;
    h = h_;
    c = c_;
  }

  boolean contains(Walker mass_) 
    {
    PVector l = mass_.position;
    if (l.x > x && l.x < x + w && l.y > y && l.y < y + h) {
      return true;
    } 
    else 
    {
      return false;
    }
  }

  PVector drag(Walker mass_) 
  {
    float speed = mass_.velocity.mag();
    float dragMagnitude = c * speed * speed;

    PVector drag = mass_.velocity.copy();
    drag.mult(-1);
    drag.setMag(dragMagnitude);
    return drag;
  }

  void render() 
  {
    noStroke();
    fill(28, 120, 186);
    rect(x, y, w, h);
  }
}
