void setup() 
{
  size(1280, 720);
  
  for (int i = 0; i < walker.length; i++) 
  {
  walker[i] = new Walker(random(1, 7), 30 + i * 150, 5);
  walker[i].mass = 9 - i;
  }
  
  liquid = new Liquid(0, height/2, width, height/2, 0.1);
}

Walker[] walker = new Walker[8];
Liquid liquid;

void draw() 
{
  background(255);
  liquid.render();

  for (Walker w: walker) 
  {
    if (liquid.contains(w)) 
    {

      PVector drag = liquid.drag(w);

      w.applyForce(drag);
    }

    PVector gravity = new PVector(0, 0.15f * w.mass);
    w.applyForce(gravity);
    w.update();
    w.render();
    w.checkEdges();
  }
  
  if (mousePressed && (mouseButton == LEFT))
  {
    setup();
  }
}
