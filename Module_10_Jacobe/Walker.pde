class Walker 
{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  public float r = random (0, 255), g = random (0, 255), b = random (0, 255);
  public float mass;

  Walker(float mass_, float x, float y) 
  {
    mass = mass_;
    position = new PVector(x, y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
  }

  void applyForce(PVector force) 
  {

    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }

  void update() 
  {
    velocity.add(acceleration);
    position.add(velocity);
    acceleration.mult(0);
  }

  void render() 
  {
    fill(r, g, b);
    circle(position.x, position.y, mass*16);
  }

  void checkEdges() 
  {
    if (position.y > height) 
    {
      position.y = height;
      velocity.y *= -1;  
    }
  }
}
