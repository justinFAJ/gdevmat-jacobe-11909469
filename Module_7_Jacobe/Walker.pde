class Walker
{
  float size = random (15, 30);
  PVector position = new PVector(random(width), random(height));
  PVector velocity = new PVector(0, 0);
  PVector acceleration = new PVector(0, 0);
  
  void update()
  {
    PVector mousePos = new PVector(mouseX, mouseY);
    mousePos.sub(position);
    mousePos.setMag(0.2);
    acceleration = mousePos;
    
    velocity.add(acceleration);
    position.add(velocity);
    velocity.limit(9);
    mousePos.normalize();
  }
  
  void checkEdges()
  {
    if (position.x > width)
    {
      position.x = 0;
    }
    else if (position.x < 0)
    {
      position.x = width;
    }
    if (position.y > height)
    {
      position.y = 0;
    }
    else if (position.y < 0)
    {
      position.y = height;
    }
  }
  
  void render()
  {
    stroke(1);
    strokeWeight(2);
    fill(211, 211, 211);
    circle(position.x, position.y, size);
  }
}
