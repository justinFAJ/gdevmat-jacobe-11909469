void setup()
{
  size (1280, 720, P3D);
  myWalker = new Walker();
  for (int i = 0; i < Walker.length; i++)
  {
    Walker[i] = new Walker();
  }
}

Walker myWalker;
Walker[] Walker = new Walker[100];

void draw()
{
  background(125, 125, 125);
  
  for (int i = 0; i < Walker.length; i++)
  {
    Walker[i].update();
    Walker[i].checkEdges();
    Walker[i].render();
  }
}
