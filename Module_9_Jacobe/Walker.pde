class Walker
{
  PVector position = new PVector(-600, 0);
  PVector velocity = new PVector();
  PVector acceleration = new PVector();
  
  public float r = random (0, 255), g = random (0, 255), b = random (0, 255);
  public float mew = initialMew;
  public float normal = 1;
  public float frictionMagnitude = mew * normal;
  public float velocityLimit = 30;
  public float scale = 2;
  public float mass;
  
  Walker(int index)
  {
    mass = 4 + index;
    scale = mass * 8;
    position.y = (Window.top - ((Window.windowHeight/8) * (index + 1 ))) + ((Window.windowHeight/8)/2);
  }
  
  public void applyForce(PVector force)
  {
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
  
  public void update()
  {
    float normal = 1;
    float frictionMagnitude = this.mew * normal;
    PVector friction = this.velocity.copy();
    
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    this.applyForce(friction);
    
    this.velocity.add(acceleration);
    this.position.add(velocity);
    this.velocity.limit(velocityLimit);
    this.acceleration.mult(0);
    
    midBrake();
  }
  
  void midBrake()
  {
    if (this.position.x + this.scale / 2 > 0)
    {
      this.mew = newFriction;
    }
  }
  
  void render()
  {
    noStroke();
    fill(r, g, b);
    circle(position.x, position.y, scale);
  }
  
}
