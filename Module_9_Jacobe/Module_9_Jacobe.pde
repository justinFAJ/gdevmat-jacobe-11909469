void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < walker.length; i++)
  {
    walker[i] = new Walker(i);
  }
}

Walker[] walker = new Walker[8];
float newFriction = 0.4f;
float initialMew = 0.01f;

void draw()
{
  background(255);
  
  for (Walker w: walker)
  {
    PVector acceleration = new PVector(0.2, 0);
    w.render();
    w.update();
    w.applyForce(acceleration);
  }
  
  stroke(0);
  strokeWeight(8);
  line(0, Window.top, 0, Window.bottom);
  
  if (mousePressed && (mouseButton == LEFT))
  {
    setup();
  }
}
