void setup()
{
  size(1280, 720, P3D);
  
  for (int i = 0; i < walker.length; i++)
  {
    walker[i] = new Walker(random(1, 6), random(width), random(height));
  }
}

Walker[] walker = new Walker [10];
float f = 0.4;

void draw()
{
  background(255);
  
  for (int i = 0; i < walker.length; i++)
  {
    for (int i_ = 0; i_ < walker.length; i_++)
    {
      if (i != i_)
      {
        PVector force = walker[i_].calculateAttraction(walker[i]);
        walker[i].applyForce(force);
      }
    }
    walker[i].update();
    walker[i].render();
  }
   println(frameCount);
   if (frameCount == 1500)
   {
     setup();
   }
}
