class Walker
{
  public float r = random (0, 255), g = random (0, 255), b = random (0, 255);
  PVector position;
  PVector velocity;
  PVector acceleration;
  public float mass = 1;
  
  Walker(float m, float x, float y)
  {
    mass = m; 
    position = new PVector(x, y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
  }
  
  void applyForce(PVector force)
  {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }
  
  void update()
  {
    velocity.add(acceleration);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void render()
  {
    noStroke();
    fill(r, g, b, 80);
    circle(position.x, position.y, mass * 40);
  }
  
  public PVector calculateAttraction(Walker walker)
  {
    PVector force = PVector.sub(position, walker.position);
    float distance = force.mag();
    distance = constrain(distance, 0.75, 5);
    force.normalize();
    
    float strength = (f * mass * walker.mass)/(distance * distance);
    force.mult(strength);
    return force;
  }
}
